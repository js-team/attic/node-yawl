Source: node-yawl
Maintainer: Debian Javascript Maintainers <pkg-javascript-devel@lists.alioth.debian.org>
Uploaders: Andrew Kelley <superjoe30@gmail.com>
Section: javascript
Testsuite: autopkgtest-pkg-nodejs
Priority: optional
Build-Depends: debhelper (>= 11~),
               dh-buildinfo,
               mocha,
               nodejs,
               node-pend (>= 1.2.0),
               node-bl,
               pkg-js-tools
Standards-Version: 4.3.0
Vcs-Browser: https://salsa.debian.org/js-team/node-yawl
Vcs-Git: https://salsa.debian.org/js-team/node-yawl.git
Homepage: https://github.com/andrewrk/node-yawl

Package: node-yawl
Architecture: all
Depends: ${misc:Depends},
         nodejs,
         node-pend (>= 1.2.0),
         node-bl
Description: yet another web sockets library
 yawl is a Node.js module which provides an API to create a web socket server
 and a web socket client. It is RFC 6455 compliant with two exceptions:
 .
 First, it uses Node.js's built in UTF-8 decoding instead of strictly closing
 the connection when decoding errors occur.
 .
 Second, the "payload length" field is limited to 2 to the power of 52 instead
 of 2 to the power of 64.
 .
 yawl uses streams and handles backpressure correctly. It is a pure JavaScript
 implementation.
 .
 Node.js is an event-based server-side JavaScript engine.
